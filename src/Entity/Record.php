<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RecordRepository")
 */
class Record
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $CategoryLabel;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCategoryLabel(): ?string
    {
        return $this->CategoryLabel;
    }

    public function setCategoryLabel(string $CategoryLabel): self
    {
        $this->CategoryLabel = $CategoryLabel;

        return $this;
    }
}
