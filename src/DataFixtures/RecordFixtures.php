<?php

namespace App\DataFixtures;

use App\Entity\Record;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class RecordFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);

        for ($i = 0, $samplingRate = 256; $i < $samplingRate; $i++) {
            $record = new Record();
            $record->setCategoryLabel(md5(rand(0, $i + 1)));
            $manager->persist($record);
        }

        $manager->flush();
    }
}
