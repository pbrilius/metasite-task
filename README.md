# Project

## Outline

[www.metasite.net](Metasite) test code task, evaluating coding solution capabilities and transacted vectors in
terms of shipping and deployment. Conforming to Symfony PSR series standards, implementing Twitter Bootstrap;
adhering to frontend management and development technologies like Webpack.js, formulated in *JS standard* and *ECMA*.

## Installation

Install Symfony binary (https://symfony.com/download), npm and yarn. Prerequisites and dependencies for *production*:

```sh
composer install
yarn install
./bin/console doctrine:fixtures:load -e prod
cp -v .env.prod .prod
```

Production mode and *TSL* certificates:

```sh
symfony local:server:ca:install
yarn encore production
```

The latter command activates **production** environment.

## Setup and run

```sh
symfony local:server:start -d
symfony local:server:prod
symfony local:server:log
```

## Browse the staged CRUD website

https://127.0.0.1:8000/, https://127.0.0.1:8000/record/

**Login** with
- demo@metasite.net
- *metasite.net*

### Important

Don't forget to ignore browser staged **insecure SSL/TLS** *certificate errors*, because, they are OS trusted, but Browser **not yet** *by default*. 

## Debugging in case of 500 errors

```sh
tail -f var/log/prod.log
```

## Testing

Ensure server is running by:

```sh
symfony local:server:status
```

Distribute the tests pack:

```sh
cp -v phpunit.xml.dist phpunit.xml
```

Run tests:

```sh
./bin/phpunit
```
