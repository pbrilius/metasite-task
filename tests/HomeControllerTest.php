<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class HomeControllerTest extends WebTestCase
{
    public function testHomePageContainerHeader()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', 'https://127.0.0.1:8000/');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorExists('h1');
        $this->assertGreaterThan(
            0,
            $crawler
                ->filter('html div.example-wrapper h1:contains("Hello HomeController!")')
                ->count()
        );
        
    }
}
